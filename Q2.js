/* name: Dylan Hodder
   ID: 30002799
   date: 29/03/2018
*/   

var occur;                                                          //declaring variables.
var userNum;
var average;
var numbers = 0;                                                    //assinging numbers a 0 to be recognised as an integer. This integer is not fixed and can be changed at any time once requested.

for(occur = 1; occur <= 10; occur++){                               //this curly bracket is the beginning of the loop. Running the code below 10 times. occur is set to 1 so as long as occur is less than or equal to 10, the code will keep running. occur++ adds 1 onto occur each time the loop runs.
    userNum = Number(prompt("Enter a number."));                    //userNum is the prompt which the user can enter the numbers.
    numbers = (numbers + userNum);                                  //numbers variable used to hold the numbers being entered and adds the user input number to the variable each time the loop runs.
    console.log(`Number ${occur}: ${userNum}`)                      //printing the numbers inputted by the user to the console each time the loop runs.
}                                                                   //this curly bracket is the end of the loop.

average = Number((numbers / 10));                                   //assigning the average variable the total of the numbers inputted and dividing them by 10 to get the average.
console.log(`The average of the numbers entered is ${average}`);    //printing the average to the console with a message.