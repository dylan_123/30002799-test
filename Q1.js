/*name: Dylan Hodder
  ID: 30002799
  Date: 29/03/2018
*/

var userInput = 0;                                                                  //declaring the variables as integers as they will be assigned numbers.
var Fahrenheit = 0;

console.log("Degrees celsius will be converted to fahrenheit.")                     //printing this string to the console.

userInput = prompt("Enter a celsius degree to be converted to Fahrenheit.")         //pops up a prompt in the browser for the user to enter a number for a celsius degree.
Fahrenheit = Number(userInput * 18 / 10 + 32);                                      //assigned a calculation to convert celsius to fahrenheit to the Fahrenheit variable. Number is used to recognise the calculation as integers.
console.log(`${userInput} degrees celsius is ${Fahrenheit} degrees fahrenheit.`);   //printing the celsius user input to the screen with the fahrenheit conversion, and a sentence explaining. This is called concatenation.