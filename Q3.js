/* name: Dylan Hodder
   ID: 30002799
   date: 29/03/2018
*/

var id;                                                                                     //declaring variables.
var name;
var units = 0;                                                                              //assinging units a number to be recognised as an integer.
var total = 0;

id = prompt("What is your customer ID?");                                                   //prompt for the user to input their ID number.
name = prompt("What is your name?");                                                        //prompt for the user to input their name.
units = prompt(`How many units have you used, ${name}.`);                                   //prompt for the user to input the units they've used. This prompt ask them this and include their name.

console.log(`User ID: ${id}\nName: ${name} \nUnits used: ${units}`);                        //prints the ID, name and units they've used. These three variables are each on a new line using '\n'. Referencing is used '${}' to display the variable information.

if(units < 200){                                                                            //the if and else statements are used for condition checking. example: if the units the user used is less than 200, the cost per unit will be $1.20. The same rule applies to each if and else statement.
    total = (units * 1.20);                                                                 //the total cost of all units used is assigned to the total variable.
    console.log(`You owe $${total.toFixed(2)} for using ${units} units at $1.20 per unit.`);
}else if(units >= 200 && units < 400){
    total = (units * 1.50);
    console.log(`You owe $${total.toFixed(2)} for using ${units} units at $1.50 per unit.`);
}else if(units >= 400 && units < 600){
    total = (units * 1.80);
    console.log(`You owe $${total.toFixed(2)} for using ${units} units at $1.80 per unit.`);
}else if(units >= 600){
    total = (units * 2.00);
    console.log(`You owe $${total.toFixed(2)} for using ${units} units at $2.00 per unit.`);
}