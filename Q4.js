/* name: Dylan Hodder
   ID: 30002799
   date: 29/03/2018
*/

var userInput;                                         //declaring user input as variable.

userInput = prompt("Enter a letter of the alphabet.");//user input has a prompt assigned to it to input letter of alphabet.

switch(userInput){                                    //switch statement switches between the cases (strings) to find which one was entered for userInput.
    case "a":                                         //these cases are being checked to see if they match the userInput letter.
    case "e":
    case "i":
    case "o":
    case "u":
        console.log(`${userInput} is a vowel.`)        //prints the userInput letter to the console and the message.
        break;                                         //break is the end of the switching for the cases above it.
    case "b":                                          //these cases are being checked to see if they match the userInput letter.
    case "c":
    case "d":
    case "f":
    case "g":
    case "h":
    case "j":
    case "k":
    case "l":
    case "m":
    case "n":
    case "p":
    case "q":
    case "r":
    case "s":
    case "t":
    case "v":
    case "w":
    case "x":
    case "y":
    case "z":
        console.log(`${userInput} is a consonant.`);    //printing the userInput letter and message to console.
        break;                                          //end of the switch statements above break.
}